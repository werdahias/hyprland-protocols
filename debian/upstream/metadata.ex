# Example file for upstream/metadata.
# See https://wiki.debian.org/UpstreamMetadata for more info/fields.
# Below an example based on a github project.

# Bug-Database: https://github.com/<user>/hyprland-protocols/issues
# Bug-Submit: https://github.com/<user>/hyprland-protocols/issues/new
# Changelog: https://github.com/<user>/hyprland-protocols/blob/master/CHANGES
# Documentation: https://github.com/<user>/hyprland-protocols/wiki
# Repository-Browse: https://github.com/<user>/hyprland-protocols
# Repository: https://github.com/<user>/hyprland-protocols.git
